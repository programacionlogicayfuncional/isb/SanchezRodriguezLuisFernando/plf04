;1
(defn string-E-1
  [x]
  (letfn [(f [x y]
            (if (empty? x)
              (if (and (> y 0) (< y 4))
                true false)
              (if (= \e (first x))
                (f (rest x) (inc y))
                (f (rest x) y))))]
    (f x 0)))

(string-E-1 "Hello")
(string-E-1 "Heelle")
(string-E-1 "Heelele")
(string-E-1 "Hll")
(string-E-1 "e")
(string-E-1 "")


(defn string-E-2
  [a]
  (letfn [(f [s acc]
            (if (zero? (count s))
              (and (>= acc 1) (<= acc 3))
              (if (= (first s) \e)
                (f (rest s) (inc acc))
                (f (rest s) acc))))]
    (f a 0)))

(string-E-2 "Hello")
(string-E-2 "Heelle")
(string-E-2 "Heelele")
(string-E-2 "Hll")
(string-E-2 "e")
(string-E-2 "")

;2
(defn string-times-1
  [s n]
  (if (== n 0)
    ""
    (str s (string-times-1 s (dec n)))))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [s n acc]
  (if (== n 0)
    acc
    (string-times-2 s (dec n) (str s acc))))

(string-times-2 "Hi" 2 "")
(string-times-2 "Hi" 3 "")
(string-times-2 "Hi" 1 "")
(string-times-2 "Hi" 0 "")
(string-times-2 "Hi" 5 "")
(string-times-2 "Oh Boy!" 2 "")
(string-times-2 "x" 4 "")
(string-times-2 "" 4 "")
(string-times-2 "code" 2 "")
(string-times-2 "code" 3 "")

;3
(defn front-times-1
  [s n]
  (letfn [(f [x y]
            (if (< (count x) 3)
              (if (zero? y)
                ""
                (str s (f x (dec y))))
              (if (zero? y)
                ""
                (str (subs s 0 3) (f x (dec y))))))]
    (f s n)))

(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

(defn front-times-2
  [s n acc]
  (letfn [(f [x y w]
            (if (< (count x) 3)
              (if (zero? y)
                w
                (f x (dec y) (str x w)))
              (if (zero? y)
                w
                (f x (dec y) (str (subs x 0 3) w)))))]
    (f s n acc)))

(front-times-2 "Chocolate" 2 "")
(front-times-2 "Chocolate" 3 "")
(front-times-2 "Abc" 3 "")
(front-times-2 "Ab" 4 "")
(front-times-2 "A" 4 "")
(front-times-2 "" 4 "")
(front-times-2 "Abc" 0 "")

;4
(defn count-xx-1
  [s]
  (if (empty? s)
    0
    (let [count-xx-1 (fn [p] (= p [\x \x]))]
      (count (filter count-xx-1 (map vector s (rest s)))))))


(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")
(count-xx-1 "kittens")
(count-xx-1 "kittensxxx")



(defn count-xx-2
  [s acc]
  (if (empty? s)
    acc
    (+ acc (let [count-xx-1 (fn [p] (= p [\x \x]))]
             (count (filter count-xx-1 (map vector s (rest s))))))))

(count-xx-2 "abcxx" 0)
(count-xx-2 "xxx" 0)
(count-xx-2 "xxxx" 0)
(count-xx-2 "abc" 0)
(count-xx-2 "hello there" 0)
(count-xx-2 "Hexxo thxxe" 0)
(count-xx-2 "" 0)
(count-xx-2 "kittens" 0)
(count-xx-2 "kittensxxx" 0)

;5

(defn string-splosion-1
  [x]
  (letfn [(f [y]
            (if (== 0 (count y))
              y
              (apply str (f (subs y 0 (- (count y) 1))) y)))]
    (f x)))

(string-splosion-1 "Code" )
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")


(defn string-splosion-2
  [z ac]
  (letfn [(f [s acc]
            (if (== 0 (count s))
              acc
              (f (subs s 0 (- (count s) 1)) (str s acc))))]
    (f z ac)))

(string-splosion-2 "Code" "")
(string-splosion-2 "abc" "")
(string-splosion-2 "ab" "")
(string-splosion-2 "x" "")
(string-splosion-2 "fade" "")
(string-splosion-2 "There" "")
(string-splosion-2 "kitten" "")
(string-splosion-2 "Bye" "")
(string-splosion-2 "Good" "")
(string-splosion-2 "Bad" "")

;6

(defn array123-1
  [xs]
  (letfn [(f [ys]
            (if (and (= 1 (first ys))
                     (= 2 (first (rest ys)))
                     (= 3 (first (rest (rest ys)))))
              true (if (empty? ys) false (f (rest ys)))))]

    (f xs)))

(array123-1 [1,1,2,3,1])
(array123-1 [1,1,2,4,1])
(array123-1 [1,1,2,3,1])
(array123-1 [1,1,2,1,2,3])
(array123-1 [1,1,2,1,2,1])
(array123-1 [1,2,3,1,2,3])
(array123-1 [1,2,3])
(array123-1 [1,1,1])
(array123-1 [1,2])
(array123-1 [1])
(array123-1 [])


(defn array123-2
  [ys]
  (letfn [(f [xs acc]
            (if (empty? xs)
              (str acc false)
              (if (>= (count xs) 3)
                (if (and (== (first xs) 1) (== (first (rest xs)) 2) (== (first (rest (rest xs))) 3))
                  (str acc true)
                  (f (rest xs) acc))
                (f (empty xs) acc))))]
    (f ys "")))

(array123-2 [1,1,2,3,1])
(array123-2 [1,1,2,4,1])
(array123-2 [1,1,2,3,1])
(array123-2 [1,1,2,1,2,3])
(array123-2 [1,1,2,1,2,1])
(array123-2 [1,2,3,1,2,3])
(array123-2 [1,2,3])
(array123-2 [1,1,1])
(array123-2 [1,2])
(array123-2 [1])
(array123-2 [])


;7

(defn stringX-1
  [x]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (== 0 z)
                (str (first y) (f (rest y) (inc z)))
                (if (and (= \x (first y)) (> (count y) 1))
                  (f (rest y) (inc z))
                  (str (first y) (f (rest y) (inc z)))))))]
    (f x 0)))


(stringX-1 "xxHxix")
(stringX-1 "abxxxcd")
(stringX-1 "xabxxxcdx")
(stringX-1 "xKittenx")
(stringX-1 "Hello")
(stringX-1 "xx")
(stringX-1 "x")
(stringX-1 "")

(defn stringX-2
  [x]
  (letfn [(f [x y acc]
            (if (empty? x)
              acc
              (if (== 0 y)
                (str (first x) (f (rest x) (inc y) (str acc)))
                (if (and (= \x (first x)) (> (count x) 1))
                  (f (rest x) (inc y) (str acc))
                  (str (first x) (f (rest x) (inc y) acc))))))]
    (f x 0 "")))

(stringX-2 "xxHxix")
(stringX-2 "abxxcd")
(stringX-2 "xabxxxcdx")
(stringX-2 "xKittenx")
(stringX-2 "Hello")
(stringX-2 "xx")
(stringX-2 "xx")
(stringX-2 "x")
(stringX-2 "")

;8
(defn altPairs-1
  [x]
  (letfn [(f [y z]
            (if (== (count y) z)
              ""
              (if (or (== z 0) (== z 1) (== z 4) (== z 5) (== z 8) (== z 9))
                (str (subs y z (+ z 1)) (f x (inc z)))
                (f x (inc z)))))]
    (f x 0)))

(altPairs-1 "kitten")
(altPairs-1 "Chocolate")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")

(defn altPairs-2
  [a]
  (letfn [(f [x i acc]
            (if (== (count x) i)
              acc
              (if (or (== i 0) (== i 1) (== i 4) (== i 5) (== i 8) (== i 9))
                (f x (inc i) (str acc (subs x i (+ i 1))))
                (f x (inc i) acc))))]
    (f a 0 "")))

(altPairs-2 "kitten")
(altPairs-2 "Chocolate")
(altPairs-2 "CodingHorror")
(altPairs-2 "yak")
(altPairs-2 "ya")
(altPairs-2 "")
(altPairs-2 "ThisThatTheOTher")





;9 yak
(defn string-Yak-1
  [s]
  (letfn [(f [x]
            (if (empty? x)
              ""
              (if (and (= (first x) \y) (= (first (rest (rest x))) \k) (> (count x) 1))
                (f (rest (rest (rest x))))
                (str (first x) (f (rest x))))))]
    (f s)))


(string-Yak-1 "yakpak")
(string-Yak-1 "pakyak")
(string-Yak-1 "yak123ya")
(string-Yak-1 "yak")
(string-Yak-1 "yakxxxyak")
(string-Yak-1 "HiyakHi")
(string-Yak-1 "xxxyakyyyakzzz")

(defn string-Yak-2
  [x]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (and (= (first y) \y) (= (first (rest (rest y))) \k) (> (count y) 1))
                (f (rest (rest (rest y))) (inc z))
                (str (first y) (f (rest y) (inc z))))))]
    (f x 0)))


(string-Yak-2 "yakpak")
(string-Yak-2 "pakyak")
(string-Yak-2 "yak123ya")
(string-Yak-2 "yak")
(string-Yak-2 "yakxxxyak")
(string-Yak-2 "HiyakHi")
(string-Yak-2 "xxxyakyyyakzzz")

;10
(defn has271
  [xs]
  (letfn [(f [ys]
            (if (or (<= (count ys) 2) (empty? ys))
              false
              (if (and
                   (== (first (rest ys)) (+ (first ys) 5))
                   (<= (if (pos? (- (first (rest (rest ys))) (dec (first ys))))
                         (- (first (rest (rest ys))) (dec (first ys)))
                         (* -1 (- (first (rest (rest ys))) (dec (first ys))))) 2))
                true
                (f (rest ys)))))]
    (f xs)))

(has271 [1,2,7,1])
(has271 [1, 2, 8, 1])
(has271 [2, 7, 1])
(has271 [3, 8, 2])
(has271 [2, 7, 3])
(has271 [2, 7, 4])
(has271 [2, 7, -1])
(has271 [2, 7, -2])
(has271 [4, 5, 3, 8, 0])
(has271 [2, 7, 5, 10, 4])
(has271 [2, 7, -2, 4, 9, 3])
(has271 [2, 7, 5, 10, 1])
(has271 [2, 7, -2, 4, 10, 2])
(has271 [1, 1, 4, 9, 0])
(has271 [1, 1, 4, 9, 4, 9, 2])

(defn has-271-2
  [x]
  (letfn [(f [y acc]
            (if (empty? y)
              false
              (if (>= (count y) 3)
                (if (and (== (- (first (rest y)) (first y)) 5) (<= (if (< (- (first (rest (rest y))) (- (first y) 1)) 0)
                                                                     (* -1 (- (first (rest (rest y))) (- (first y) 1)))
                                                                     (* 1 (- (first (rest (rest y))) (- (first y) 1))))  2))
                  true
                  (f (rest y) acc))
                (f (empty y) acc))))]
    (f x "")))

(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 1])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2 4 9 3])
(has-271-2 [2 7 5 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])